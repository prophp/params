<?php

use ProPhp\Params\Readme\Examples\CustomParams;

# DEFAULT VALUES

$Params = new CustomParams();

if ($Params->isSilent() !== true)
    throw new Exception("Invalid default value for 'validate'");

if ($Params->wildcard() !== null)
    throw new Exception("Invalid default value for 'wildcard'");

if ($Params->daysToKeepBackups() !== 10)
    throw new Exception("Invalid default value for 'daysToKeepBackups'");


# SETTERS & GETTERS

$errorCaught = false;

try {
    $Params = (new CustomParams())
        ->isSilent()
        ->wildcard();
} catch (Error $e) {
    $errorCaught = true;
}

if (!$errorCaught)
    throw new Exception("Expected Fatal error was not thrown");

if ($e->getMessage() !== 'Call to a member function wildcard() on bool')
    throw new Exception(
        "Fatal error message '{$e->getMessage()}' is not same as 'Call to a member function wildcard() on bool'"
    );


$Params = (new CustomParams())
    ->isSilent(false)
    ->wildcard("*.log")
    ->daysToKeepBackups(0);


if ($Params->isSilent() !== false)
    throw new Exception("Invalid value for 'validate'");

if ($Params->wildcard() !== "*.log")
    throw new Exception("Invalid value for 'wildcard'");

if ($Params->daysToKeepBackups() !== 0)
    throw new Exception("Invalid value for 'daysToKeepBackups'");