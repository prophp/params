# ◦ Requirements

- PHP 8.0+

# ◦ Install

## `composer require prophp/params`

# ◦ Create manually

▸ Take a look at [README/Examples](https://gitlab.com/prophp/params/-/tree/master/README/Examples) folder.

`JsonParams.php` extends `ProPhp\Params`

▸ Create same `...Params` classes manually or ↓

# ◦ Render 

▸ Install `...Params` renderer

`composer require prophp/render-params --dev`

▸ Take a look at [README/Examples](https://gitlab.com/prophp/params/-/tree/master/README/Examples) folder.

`JsonParams.render` file was created manually

▸ Afterwards a terminal command `bin/render params` was run 
from a project's root (where `composer.json` file is located)

`JsonParams.php` file was generated automatically in the same folder 
where `JsonParams.render` file is located

NB! `namespace` is also generated automatically 🙂

# ◦ USE

▸ Take a look at [tests/UnitTests/Params.php](https://gitlab.com/prophp/params/-/blob/master/tests/UnitTests/Params.php) file.

# ◦ TEST

root ▸ `docker/stop-all` - optional

root ▸ `docker/run` - required only if your local PHP version is lower than 8.0

docker `/var/www/html` ▸ `php tests/UnitTests/Params.php`

root ▸ `docker/stop` || `docker/stop-all` - required only if your local PHP version is lower than 8.0

### `bin/test-lite` @todo LATER